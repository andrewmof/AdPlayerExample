package com.afomenko.europaplus

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.media3.common.*
import androidx.media3.common.C.TRACK_TYPE_AUDIO
import androidx.media3.common.util.UnstableApi
import androidx.media3.common.util.Util
import androidx.media3.datasource.DataSource
import androidx.media3.datasource.DefaultDataSource
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.ima.ImaAdsLoader
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import androidx.media3.exoplayer.source.MediaSource
import androidx.multidex.MultiDex
import com.afomenko.europaplus.databinding.ActivityEpBinding
import com.google.ads.interactivemedia.v3.api.*
import com.google.ads.interactivemedia.v3.api.AdEvent.AdEventListener
import com.google.ads.interactivemedia.v3.api.AdEvent.AdEventType
import java.util.*


class EpActivity : AppCompatActivity() {

    private val SAMPLE_VAST_TAG_IMG =
        "https://a.adwolf.ru/3145/getCode?p1=bdfe&p2=jf&pfc=zhn&pfb=dtpg"
    private val SAMPLE_VAST_TAG_VIDEO =
            "https://a.adwolf.ru/3145/getCode?p1=bdfe&p2=jf&pfc=zhn&pfb=dsrn";
    private val SAMPLE_VAST_TAG_HTML =
      "https://a.adwolf.ru/3145/getCode?p1=bdfe&p2=jf&pfc=zhn&pfb=dtpr";
    private val SAMPLE_VAST_TAG_HTMLC =
    "http://a.adwolf.ru/3145/getCode?p1=bdfe&p2=jf&pfc=zhn&pfb=dtps";
    private val LOG_TAG = "ImaEPplayerExample"
    lateinit var logText: TextView
    private var adsLoader: ImaAdsLoader? = null
    private var mAdsLoader: AdsLoader? = null
    private var player: ExoPlayer? = null
    private var playWhenReady = false
    private var currentItem = 0
    private var playbackPosition = 0L


    private val viewBinding by lazy(LazyThreadSafetyMode.NONE) {
        ActivityEpBinding.inflate(layoutInflater)
    }
    @UnstableApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        MultiDex.install(this)

        val sdkFactory = ImaSdkFactory.getInstance()
        val companionViewGroup = viewBinding.companionAdSlot as ViewGroup
        val companionAdSlot = sdkFactory.createCompanionAdSlot()
        companionAdSlot.container = companionViewGroup
        companionAdSlot.setSize(720, 720)
        val companionAdSlots = ArrayList<CompanionAdSlot>()
        companionAdSlots.add(companionAdSlot)

        val el1 = UiElement.AD_ATTRIBUTION
        val el2 = UiElement.COUNTDOWN
        // Create an AdsLoader.
        adsLoader = ImaAdsLoader.Builder( this)
            .setAdMediaMimeTypes(listOf("audio/mp3", "video/mp4","image/jpeg","image/gif","image/png"))
            .setCompanionAdSlots(companionAdSlots)
            .setAdUiElements(setOf(el1,el2))
            .setAdEventListener(buildAdEventListener())
            .build()

    }
    @UnstableApi
    private fun buildAdEventListener(): AdEventListener {
        return (AdEventListener { event: AdEvent ->
            val eventType = event.type
          //  Log.i(LOG_TAG, "IMA event: $eventType" )
            if (eventType != AdEventType.AD_PROGRESS) {
                if (eventType == AdEventType.ALL_ADS_COMPLETED){
                    val image = ImageView(this)
                    image.layoutParams =
                        LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
                    image.setBackgroundResource(R.drawable.ic_launcher_background)
                    image.imageTintMode = null;
                    image.setImageDrawable(
                        ContextCompat.getDrawable(this, R.mipmap.ic_launcher_def))
                    viewBinding.companionAdSlot.removeAllViews()
                    viewBinding.companionAdSlot.addView(image)
                    viewBinding.companionAdSlot.invalidate()

                }else
                    if (eventType == AdEventType.STARTED &&
                        event.ad != null &&
                        event.ad.contentType  != null &&
                        event.ad.contentType != "video/mp4"
                            ){
                        viewBinding.companionAdSlot.visibility = LinearLayout.VISIBLE;
                    }
            }

        })
    }

    @UnstableApi
    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }
    @UnstableApi
    override fun onResume() {
        super.onResume()
            //   hideSystemUi()
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer()
        }

    }
    @UnstableApi
    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
        }

    }
    @UnstableApi
    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, viewBinding.playerView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    @UnstableApi
    private fun initializePlayer() {

        // Set up the factory for media sources, passing the ads loader and ad view providers.
        val dataSourceFactory: DataSource.Factory = DefaultDataSource.Factory(this)

        val mediaSourceFactory: MediaSource.Factory = DefaultMediaSourceFactory(dataSourceFactory)
            .setLocalAdInsertionComponents(
                { adsLoader },
                viewBinding.playerView
            )


        player = ExoPlayer.Builder(this)
            .setMediaSourceFactory(mediaSourceFactory)
            .build()
            .also { exoPlayer ->
                viewBinding.playerView.player = exoPlayer
                viewBinding.playerView.setShowRewindButton(false)
                viewBinding.playerView.setShowFastForwardButton(false)
                viewBinding.playerView.setControllerHideDuringAds(false)
                viewBinding.playerView.controllerShowTimeoutMs = 0
                adsLoader?.setPlayer(exoPlayer)
                val adTagUri =
                    Uri.parse(SAMPLE_VAST_TAG_IMG)
                val plus = MediaItem.Builder()
                    .setUri(getString(R.string.europa_plus))
                    .setMimeType(MimeTypes.APPLICATION_M3U8)
                    .setAdsConfiguration(MediaItem.AdsConfiguration.Builder(adTagUri).build())
                    .build()
                val top40 = MediaItem.Builder()
                    .setUri(getString(R.string.europa_top40))
                    .setMimeType(MimeTypes.APPLICATION_M3U8)
                    .setAdsConfiguration(MediaItem.AdsConfiguration.Builder(Uri.parse(SAMPLE_VAST_TAG_VIDEO)).build())
                    .build()
                val party = MediaItem.Builder()
                    .setUri(getString(R.string.europa_party))
                    .setAdsConfiguration(MediaItem.AdsConfiguration.Builder(Uri.parse(SAMPLE_VAST_TAG_IMG)).build())
                    .setMimeType(MimeTypes.APPLICATION_M3U8)
                    .build()
                val light = MediaItem.Builder()
                    .setUri(getString(R.string.europa_light))
                    .setAdsConfiguration(MediaItem.AdsConfiguration.Builder(adTagUri).build())
                    .setMimeType(MimeTypes.APPLICATION_M3U8)
                    .build()
                exoPlayer.setMediaItem(plus)
                exoPlayer.addMediaItem(top40)
                exoPlayer.addMediaItem(party)
                exoPlayer.addMediaItem(light)
                exoPlayer.addListener(
                    object: Player.Listener {
                        private var seenAdsLoader: Boolean = false
                        override fun onTracksChanged(track: Tracks) {
                            Log.i(LOG_TAG, "onMediaItemTransition: " + track.containsType(TRACK_TYPE_AUDIO))
                        }
                        override fun onTimelineChanged(timeline: Timeline, reason: Int) {
                            if ( reason == Player.TIMELINE_CHANGE_REASON_SOURCE_UPDATE){
                                if( !seenAdsLoader &&
                                    timeline.getPeriod( 0, Timeline.Period()).adGroupCount
                                > 0) {
                                seenAdsLoader = true
                                    Log.i(LOG_TAG, "Ads loader: 1," + timeline.getPeriod(0, Timeline.Period()).adGroupCount);
                                }else Log.i(LOG_TAG, "Ads loader: 2," + timeline.getPeriod(0, Timeline.Period()).adGroupCount);
                            }
                        }
                })
                exoPlayer.playWhenReady = playWhenReady
                exoPlayer.seekTo(currentItem, playbackPosition)
                exoPlayer.prepare()
                // Set PlayWhenReady. If true, content and ads will autoplay.
                exoPlayer!!.playWhenReady = false
            }

    }
    private fun releasePlayer() {
        player?.let { exoPlayer ->
            playbackPosition = exoPlayer.currentPosition
            currentItem = exoPlayer.currentMediaItemIndex
            playWhenReady = exoPlayer.playWhenReady
            exoPlayer.release()
        }
        player = null
    }
}